package io.github.tolgadurak.currencyconversionservice.external.fixer;

import java.math.BigDecimal;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.github.tolgadurak.currencyconversionservice.api.valueobject.ExternalServiceResponseVO;
import io.github.tolgadurak.currencyconversionservice.domain.enums.Currency;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@JsonInclude(Include.NON_NULL)
public class FixerResponse implements ExternalServiceResponseVO {

	private static final long serialVersionUID = -5042850475947074146L;
	private boolean success;
	private Long timeStamp;
	private Currency base;
	private String date;
	private Map<Currency, BigDecimal> rates;
	private FixerError error;
	private FixerQuery query;
	private FixerInfo info;
	private BigDecimal result;

	public boolean getSuccess() {
		return Boolean.TRUE.equals(success);
	}

	public Long getTimeStamp() {
		return timeStamp;
	}

	public Currency getBase() {
		return base;
	}

	public String getDate() {
		return date;
	}

	public Map<Currency, BigDecimal> getRates() {
		return rates;
	}

	public FixerError getError() {
		return error;
	}

	public FixerQuery getQuery() {
		return query;
	}

	public FixerInfo getInfo() {
		return info;
	}

	public BigDecimal getResult() {
		return result;
	}

}
