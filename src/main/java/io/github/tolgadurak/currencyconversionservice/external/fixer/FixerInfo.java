package io.github.tolgadurak.currencyconversionservice.external.fixer;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@JsonInclude(Include.NON_NULL)
public class FixerInfo implements Serializable {

	private static final long serialVersionUID = 416105828954574106L;

	private Long timestamp;
	private BigDecimal rate;

	public Long getTimestamp() {
		return timestamp;
	}

	public BigDecimal getRate() {
		return rate;
	}

}
