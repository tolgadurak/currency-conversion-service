package io.github.tolgadurak.currencyconversionservice.external.fixer;

import java.io.Serializable;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class FixerError implements Serializable {

	private static final long serialVersionUID = -2820182938079436865L;
	private String code;
	private String type;
	private String info;

	public String getCode() {
		return code;
	}

	public String getType() {
		return type;
	}

	public String getInfo() {
		return info;
	}

}
