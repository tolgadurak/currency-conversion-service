package io.github.tolgadurak.currencyconversionservice.external.fixer;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.github.tolgadurak.currencyconversionservice.domain.enums.Currency;
/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@JsonInclude(Include.NON_NULL)
public class FixerQuery implements Serializable {

	private static final long serialVersionUID = 8959934669049210868L;
	private Currency from;
	private Currency to;
	private BigDecimal amount;

	public Currency getFrom() {
		return from;
	}

	public Currency getTo() {
		return to;
	}

	public BigDecimal getAmount() {
		return amount;
	}

}
