package io.github.tolgadurak.currencyconversionservice.external.fixer;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import io.github.tolgadurak.currencyconversionservice.CurrencyConversionServiceAppConfig;
import io.github.tolgadurak.currencyconversionservice.domain.enums.CurrencySource;
import io.github.tolgadurak.currencyconversionservice.dto.CurrencyExchangeResponseDto;
import io.github.tolgadurak.currencyconversionservice.dto.TransactionDto;
import io.github.tolgadurak.currencyconversionservice.exception.ExternalServiceFailureException;
import io.github.tolgadurak.currencyconversionservice.exception.InvalidHttpResponseException;
import io.github.tolgadurak.currencyconversionservice.exception.UncheckedIOException;
import io.github.tolgadurak.currencyconversionservice.service.impl.BaseCurrencyConversionService;
import io.github.tolgadurak.currencyconversionservice.util.HttpClientCommons;
import io.github.tolgadurak.currencyconversionservice.util.JsonConverter;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@Service
@Qualifier(value = "fixerCurrencyConversionService")
public class FixerCurrencyConversionService extends BaseCurrencyConversionService {

	private static final String LATEST = "latest";
	private static final String ACCESS_KEY = "access_key";
	private static final String BASE = "base";
	private static final String SYMBOLS = "symbols";
	private static final String FROM = "from";
	private static final String TO = "to";
	private static final String AMOUNT = "amount";
	private static final String CONVERT = "convert";
	private static final String ERROR_OCCURED_GET_REQUEST = "Error occured during GET request to URI: %s";
	private static final Logger logger = LogManager.getLogger(FixerCurrencyConversionService.class);

	public FixerCurrencyConversionService(CurrencyConversionServiceAppConfig appConfig, JsonConverter jsonConverter) {
		super(appConfig, jsonConverter);
	}

	@Override
	public CurrencyExchangeResponseDto queryExchangeRate(TransactionDto transactionDto) throws InterruptedException {
		CurrencyExchangeResponseDto responseDto = new CurrencyExchangeResponseDto();
		String responseBody = null;
		HttpResponse<String> response = null;
		List<NameValuePair> nameValuePairs = new LinkedList<>();
		nameValuePairs.add(new BasicNameValuePair(ACCESS_KEY, appConfig.getAccessKey()));
		nameValuePairs.add(new BasicNameValuePair(SYMBOLS, transactionDto.getToCurrency().name()));
		if (appConfig.isPaidVersion()) {
			nameValuePairs.add(new BasicNameValuePair(BASE, transactionDto.getFromCurrency().name()));
		} else {
			logger.warn(
					"Paid version is not set in the application properties. Default currrency will be used as base.");
		}
		String queryString = URLEncodedUtils.format(nameValuePairs, "UTF-8");
		StringBuilder uriBuilder = new StringBuilder(appConfig.getBaseUrl()).append(HttpClientCommons.SLASH)
				.append(LATEST).append(HttpClientCommons.QUESTION_MARK).append(queryString);
		HttpClient httpClient = HttpClient.newHttpClient();
		HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(uriBuilder.toString())).GET().build();
		responseDto.setRequestDate(new Date());
		try {
			response = httpClient.send(httpRequest, BodyHandlers.ofString());
		} catch (IOException e) {
			throw new UncheckedIOException(String.format(ERROR_OCCURED_GET_REQUEST, uriBuilder), e,
					transactionDto.getId());
		} catch (InterruptedException e) {
			throw e;
		}
		responseDto.setResponseDate(new Date());
		if (response == null || response.body() == null) {
			throw new InvalidHttpResponseException(String.format(ERROR_OCCURED_GET_REQUEST, uriBuilder),
					transactionDto.getId());
		}
		responseBody = response.body();
		FixerResponse fixerResponse = jsonConverter.deserialize(responseBody, FixerResponse.class);
		if (!fixerResponse.getSuccess()) {
			throw new ExternalServiceFailureException("An error occurred from external service side", fixerResponse,
					transactionDto.getId());
		}
		responseDto.setSource(fixerResponse.getBase());
		responseDto.setTarget(transactionDto.getFromCurrency());
		responseDto.setRate(fixerResponse.getRates().get(transactionDto.getToCurrency()));
		responseDto.setSuccess(true);
		return responseDto;
	}

	@Override
	public CurrencyExchangeResponseDto calculateTargetAmount(TransactionDto transactionDto)
			throws InterruptedException {
		CurrencyExchangeResponseDto responseDto = new CurrencyExchangeResponseDto();
		String responseBody = null;
		HttpResponse<String> response = null;
		List<NameValuePair> nameValuePairs = new LinkedList<>();
		nameValuePairs.add(new BasicNameValuePair(ACCESS_KEY, appConfig.getAccessKey()));
		nameValuePairs.add(new BasicNameValuePair(FROM, transactionDto.getFromCurrency().name()));
		nameValuePairs.add(new BasicNameValuePair(TO, transactionDto.getToCurrency().name()));
		nameValuePairs.add(new BasicNameValuePair(AMOUNT, transactionDto.getSourceAmount().toString()));
		String queryString = URLEncodedUtils.format(nameValuePairs, "UTF-8");
		StringBuilder uriBuilder = new StringBuilder(appConfig.getBaseUrl()).append(HttpClientCommons.SLASH)
				.append(CONVERT).append(HttpClientCommons.QUESTION_MARK).append(queryString);
		HttpClient httpClient = HttpClient.newHttpClient();
		HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(uriBuilder.toString())).GET().build();
		responseDto.setRequestDate(new Date());
		try {
			response = httpClient.send(httpRequest, BodyHandlers.ofString());
		} catch (IOException e) {
			throw new UncheckedIOException(String.format(ERROR_OCCURED_GET_REQUEST, uriBuilder), e,
					transactionDto.getId());
		} catch (InterruptedException e) {
			throw e;
		}
		responseDto.setResponseDate(new Date());
		if (response == null || response.body() == null) {
			throw new InvalidHttpResponseException(String.format(ERROR_OCCURED_GET_REQUEST, uriBuilder),
					transactionDto.getId());
		}
		responseBody = response.body();
		FixerResponse fixerResponse = jsonConverter.deserialize(responseBody, FixerResponse.class);
		if (!fixerResponse.getSuccess()) {
			throw new ExternalServiceFailureException("An error occurred from external service side", fixerResponse,
					transactionDto.getId());
		}
		responseDto.setSource(fixerResponse.getQuery().getFrom());
		responseDto.setTarget(fixerResponse.getQuery().getTo());
		responseDto.setRate(fixerResponse.getInfo().getRate());
		responseDto.setSourceAmount(transactionDto.getSourceAmount());
		responseDto.setResultAmount(fixerResponse.getResult());
		responseDto.setSuccess(true);
		return responseDto;
	}

	@Override
	public CurrencySource getSource() {
		return CurrencySource.FIXER;
	}

}
