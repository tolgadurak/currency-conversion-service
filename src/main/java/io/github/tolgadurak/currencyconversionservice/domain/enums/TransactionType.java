package io.github.tolgadurak.currencyconversionservice.domain.enums;
/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public enum TransactionType {
	QUERYCURRENCYRATE, EXCHANGECALCULATION
}
