package io.github.tolgadurak.currencyconversionservice.domain.enums;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public enum TransactionStatus {
	IP("01"), // In progress
	FA("99"), // Failed
	AP("00"); // Approved

	private TransactionStatus(String code) {
		this.code = code;
	}

	private String code;

	public String getCode() {
		return code;
	}

}
