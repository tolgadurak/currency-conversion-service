package io.github.tolgadurak.currencyconversionservice.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import io.github.tolgadurak.currencyconversionservice.domain.enums.Currency;
import io.github.tolgadurak.currencyconversionservice.domain.enums.CurrencySource;
import io.github.tolgadurak.currencyconversionservice.domain.enums.TransactionStatus;
import io.github.tolgadurak.currencyconversionservice.domain.enums.TransactionType;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@Entity
public class Transaction {
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;
	private Date createdDate;
	private Date sourceRequestDate;
	private Date sourceResponseDate;
	@Enumerated(EnumType.STRING)
	private Currency fromCurrency;
	@Enumerated(EnumType.STRING)
	private Currency toCurrency;
	private BigDecimal exchangeRate;
	private BigDecimal sourceAmount;
	private BigDecimal targetAmount;
	@Enumerated(EnumType.STRING)
	private TransactionStatus status;
	@Enumerated(EnumType.STRING)
	private TransactionType type;
	@Enumerated(EnumType.STRING)
	private CurrencySource currencySource;

	public Transaction() {
		this.createdDate = new Date();
	}

	public Transaction(Builder builder) {
		this.createdDate = new Date();
		this.sourceRequestDate = builder.sourceRequestDate;
		this.sourceResponseDate = builder.sourceResponseDate;
		this.fromCurrency = builder.fromCurrency;
		this.toCurrency = builder.toCurrency;
		this.exchangeRate = builder.exchangeRate;
		this.sourceAmount = builder.sourceAmount;
		this.targetAmount = builder.targetAmount;
		this.status = builder.status;
		this.type = builder.type;
		this.currencySource = builder.currencySource;
	}

	public String getId() {
		return id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getSourceRequestDate() {
		return sourceRequestDate;
	}

	public Date getSourceResponseDate() {
		return sourceResponseDate;
	}

	public Currency getFromCurrency() {
		return fromCurrency;
	}

	public Currency getToCurrency() {
		return toCurrency;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public BigDecimal getSourceAmount() {
		return sourceAmount;
	}

	public BigDecimal getTargetAmount() {
		return targetAmount;
	}

	public TransactionStatus getStatus() {
		return status;
	}

	public TransactionType getType() {
		return type;
	}

	public CurrencySource getCurrencySource() {
		return currencySource;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setSourceRequestDate(Date sourceRequestDate) {
		this.sourceRequestDate = sourceRequestDate;
	}

	public void setSourceResponseDate(Date sourceResponseDate) {
		this.sourceResponseDate = sourceResponseDate;
	}

	public void setFromCurrency(Currency fromCurrency) {
		this.fromCurrency = fromCurrency;
	}

	public void setToCurrency(Currency toCurrency) {
		this.toCurrency = toCurrency;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public void setSourceAmount(BigDecimal sourceAmount) {
		this.sourceAmount = sourceAmount;
	}

	public void setTargetAmount(BigDecimal targetAmount) {
		this.targetAmount = targetAmount;
	}

	public void setStatus(TransactionStatus status) {
		this.status = status;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setCurrencySource(CurrencySource currencySource) {
		this.currencySource = currencySource;
	}

	public static class Builder {
		private Date sourceRequestDate;
		private Date sourceResponseDate;
		private Currency fromCurrency;
		private Currency toCurrency;
		private BigDecimal exchangeRate;
		private BigDecimal sourceAmount;
		private BigDecimal targetAmount;
		private TransactionStatus status;
		private TransactionType type;
		private CurrencySource currencySource;

		public Builder sourceRequestDate(Date sourceRequestDate) {
			this.sourceRequestDate = sourceRequestDate;
			return this;
		}

		public Builder sourceResponseDate(Date sourceResponseDate) {
			this.sourceResponseDate = sourceResponseDate;
			return this;
		}

		public Builder fromCurrency(Currency fromCurrency) {
			this.fromCurrency = fromCurrency;
			return this;
		}

		public Builder toCurrency(Currency toCurrency) {
			this.toCurrency = toCurrency;
			return this;
		}

		public Builder exchangeRate(BigDecimal exchangeRate) {
			this.exchangeRate = exchangeRate;
			return this;
		}

		public Builder sourceAmount(BigDecimal sourceAmount) {
			this.sourceAmount = sourceAmount;
			return this;
		}

		public Builder targetAmount(BigDecimal targetAmount) {
			this.targetAmount = targetAmount;
			return this;
		}

		public Builder status(TransactionStatus status) {
			this.status = status;
			return this;
		}

		public Builder type(TransactionType type) {
			this.type = type;
			return this;
		}

		public Builder currencySource(CurrencySource currencySource) {
			this.currencySource = currencySource;
			return this;
		}

		public Transaction build() {
			return new Transaction(this);
		}
	}

}
