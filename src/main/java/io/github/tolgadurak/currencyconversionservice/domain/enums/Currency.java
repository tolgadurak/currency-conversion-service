package io.github.tolgadurak.currencyconversionservice.domain.enums;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public enum Currency {
	TRY, // Turkish Lira
	USD, // American Dollar
	EUR, // Euro
	GBP; // Sterling

	public static Currency fromString(String currency) {
		for (Currency c : values()) {
			if (c.name().contentEquals(currency)) {
				return c;
			}
		}
		return null;
	}
}
