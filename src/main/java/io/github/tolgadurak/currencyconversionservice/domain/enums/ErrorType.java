package io.github.tolgadurak.currencyconversionservice.domain.enums;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public enum ErrorType {
	VALIDATION, EXTERNAL_SERVICE, INVALID_HTTP_RESPONSE, HTTP_IO_EXCEPTION;
}
