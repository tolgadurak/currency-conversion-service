package io.github.tolgadurak.currencyconversionservice.domain.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public enum CurrencySource {
	FIXER("fixer.io");

	private CurrencySource(String value) {
		this.value = value;
	}

	private String value;

	public String getValue() {
		return value;
	}

	public static CurrencySource fromString(String source) {
		for (CurrencySource currencySource : values()) {
			if (StringUtils.equals(currencySource.name(), source)) {
				return currencySource;
			}
		}
		return null;
	}
}
