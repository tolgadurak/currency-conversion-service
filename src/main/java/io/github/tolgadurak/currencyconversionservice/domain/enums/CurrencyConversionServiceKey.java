package io.github.tolgadurak.currencyconversionservice.domain.enums;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public enum CurrencyConversionServiceKey {
	FIXER;

	public static CurrencyConversionServiceKey fromString(String key) {
		for (CurrencyConversionServiceKey serviceKey : values()) {
			if (serviceKey.name().equals(key)) {
				return serviceKey;
			}
		}
		return null;
	}
}
