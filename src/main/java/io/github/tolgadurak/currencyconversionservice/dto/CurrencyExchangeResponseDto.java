package io.github.tolgadurak.currencyconversionservice.dto;

import java.math.BigDecimal;
import java.util.Date;

import io.github.tolgadurak.currencyconversionservice.domain.enums.Currency;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class CurrencyExchangeResponseDto {
	private boolean success;
	private Currency source;
	private Currency target;
	private BigDecimal sourceAmount;
	private BigDecimal rate;
	private BigDecimal resultAmount;
	private Date requestDate;
	private Date responseDate;

	public CurrencyExchangeResponseDto() {
		// Default constructor
	}

	public CurrencyExchangeResponseDto(Builder builder) {
		this.source = builder.source;
		this.target = builder.target;
		this.sourceAmount = builder.sourceAmount;
		this.rate = builder.rate;
		this.resultAmount = builder.resultAmount;
		this.requestDate = builder.requestDate;
		this.responseDate = builder.responseDate;

	}

	public boolean isSuccess() {
		return success;
	}

	public Currency getSource() {
		return source;
	}

	public Currency getTarget() {
		return target;
	}

	public BigDecimal getSourceAmount() {
		return sourceAmount;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public BigDecimal getResultAmount() {
		return resultAmount;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public Date getResponseDate() {
		return responseDate;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setSource(Currency source) {
		this.source = source;
	}

	public void setTarget(Currency target) {
		this.target = target;
	}

	public void setSourceAmount(BigDecimal sourceAmount) {
		this.sourceAmount = sourceAmount;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public void setResultAmount(BigDecimal resultAmount) {
		this.resultAmount = resultAmount;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public static class Builder {
		private Currency source;
		private Currency target;
		private BigDecimal sourceAmount;
		private BigDecimal rate;
		private BigDecimal resultAmount;
		private Date requestDate;
		private Date responseDate;

		public Builder source(Currency source) {
			this.source = source;
			return this;
		}

		public Builder target(Currency target) {
			this.target = target;
			return this;
		}

		public Builder sourceAmount(BigDecimal sourceAmount) {
			this.sourceAmount = sourceAmount;
			return this;
		}

		public Builder rate(BigDecimal rate) {
			this.rate = rate;
			return this;
		}

		public Builder resultAmount(BigDecimal resultAmount) {
			this.resultAmount = resultAmount;
			return this;
		}

		public Builder requestDate(Date requestDate) {
			this.requestDate = requestDate;
			return this;
		}

		public Builder responseDate(Date responseDate) {
			this.responseDate = responseDate;
			return this;
		}

		public CurrencyExchangeResponseDto build() {
			return new CurrencyExchangeResponseDto(this);
		}
	}

}
