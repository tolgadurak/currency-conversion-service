package io.github.tolgadurak.currencyconversionservice.service;

import io.github.tolgadurak.currencyconversionservice.domain.enums.CurrencySource;
import io.github.tolgadurak.currencyconversionservice.dto.CurrencyExchangeResponseDto;
import io.github.tolgadurak.currencyconversionservice.dto.TransactionDto;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public interface CurrencyConversionService {
	CurrencyExchangeResponseDto queryExchangeRate(TransactionDto transactionDto) throws InterruptedException;

	CurrencyExchangeResponseDto calculateTargetAmount(TransactionDto transactionDto)
			throws InterruptedException;

	CurrencySource getSource();
}
