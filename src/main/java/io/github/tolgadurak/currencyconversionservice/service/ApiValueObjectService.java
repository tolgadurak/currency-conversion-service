package io.github.tolgadurak.currencyconversionservice.service;

import java.util.List;

import io.github.tolgadurak.currencyconversionservice.api.valueobject.TransactionVO;
import io.github.tolgadurak.currencyconversionservice.dto.TransactionDto;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public interface ApiValueObjectService {
	TransactionVO convert(TransactionDto dto);
	
	List<TransactionVO> convert(List<TransactionDto> transactionDtos);
}
