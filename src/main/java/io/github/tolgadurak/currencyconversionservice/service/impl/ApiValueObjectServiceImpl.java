package io.github.tolgadurak.currencyconversionservice.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import io.github.tolgadurak.currencyconversionservice.api.valueobject.TransactionVO;
import io.github.tolgadurak.currencyconversionservice.dto.TransactionDto;
import io.github.tolgadurak.currencyconversionservice.service.ApiValueObjectService;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@Service
public class ApiValueObjectServiceImpl implements ApiValueObjectService {

	@Override
	public TransactionVO convert(TransactionDto dto) {
		return new TransactionVO.Builder().transactionId(dto.getId()).fromCurrency(dto.getFromCurrency())
				.toCurrency(dto.getToCurrency()).exchangeRate(dto.getExchangeRate()).targetAmount(dto.getTargetAmount())
				.sourceAmount(dto.getSourceAmount()).sourceRequestDate(dto.getSourceRequestDate())
				.sourceResponseDate(dto.getSourceResponseDate()).transactionDate(dto.getCreatedDate())
				.status(dto.getStatus().getCode()).currencySource(dto.getCurrencySource().getValue()).build();
	}

	@Override
	public List<TransactionVO> convert(List<TransactionDto> transactionDtos) {
		return transactionDtos.stream()
				.map(t -> new TransactionVO.Builder().transactionDate(t.getCreatedDate())
						.sourceRequestDate(t.getSourceRequestDate()).sourceResponseDate(t.getSourceResponseDate())
						.fromCurrency(t.getFromCurrency()).toCurrency(t.getToCurrency())
						.exchangeRate(t.getExchangeRate()).sourceAmount(t.getSourceAmount())
						.targetAmount(t.getTargetAmount()).build())
				.collect(Collectors.toList());
	}

}
