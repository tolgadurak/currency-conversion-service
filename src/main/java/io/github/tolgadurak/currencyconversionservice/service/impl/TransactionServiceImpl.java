package io.github.tolgadurak.currencyconversionservice.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.github.tolgadurak.currencyconversionservice.domain.Transaction;
import io.github.tolgadurak.currencyconversionservice.domain.enums.TransactionStatus;
import io.github.tolgadurak.currencyconversionservice.dto.TransactionDto;
import io.github.tolgadurak.currencyconversionservice.repository.TransactionRepository;
import io.github.tolgadurak.currencyconversionservice.service.TransactionService;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@Service
public class TransactionServiceImpl implements TransactionService {

	private TransactionRepository transactionRepository;

	@Autowired
	public TransactionServiceImpl(TransactionRepository transactionRepository) {
		this.transactionRepository = transactionRepository;
	}

	@Override
	public TransactionDto getById(String id) {
		TransactionDto transactionVO = null;
		Optional<Transaction> optionalTransaction = transactionRepository.findById(id);
		if (optionalTransaction.isPresent()) {
			Transaction transaction = optionalTransaction.get();
			transactionVO = new TransactionDto.Builder().createdDate(transaction.getCreatedDate())
					.sourceRequestDate(transaction.getSourceRequestDate())
					.sourceResponseDate(transaction.getSourceResponseDate()).fromCurrency(transaction.getFromCurrency())
					.toCurrency(transaction.getToCurrency()).exchangeRate(transaction.getExchangeRate())
					.sourceAmount(transaction.getSourceAmount()).targetAmount(transaction.getTargetAmount()).build();
		}
		return transactionVO;
	}

	@Override
	public List<TransactionDto> getAllByCreatedDateBetween(Date startDate, Date endDate) {
		List<Transaction> transactions = transactionRepository.findAllByCreatedDateBetween(startDate, endDate);
		return transactions.stream()
				.map(transaction -> new TransactionDto.Builder().createdDate(transaction.getCreatedDate())
						.sourceRequestDate(transaction.getSourceRequestDate())
						.sourceResponseDate(transaction.getSourceResponseDate())
						.fromCurrency(transaction.getFromCurrency()).toCurrency(transaction.getToCurrency())
						.exchangeRate(transaction.getExchangeRate()).sourceAmount(transaction.getSourceAmount())
						.targetAmount(transaction.getTargetAmount()).build())
				.collect(Collectors.toList());
	}

	@Override
	public Transaction addTransaction(TransactionDto transactionDto) {
		Transaction transaction = new Transaction.Builder().fromCurrency(transactionDto.getFromCurrency())
				.toCurrency(transactionDto.getToCurrency()).sourceAmount(transactionDto.getSourceAmount())
				.status(transactionDto.getStatus()).type(transactionDto.getType()).build();
		return transactionRepository.save(transaction);
	}

	@Override
	public Transaction updateTransaction(String id, TransactionDto transactionDto) {
		Optional<Transaction> optionalTransaction = transactionRepository.findById(id);
		if (optionalTransaction.isPresent()) {
			Transaction transaction = optionalTransaction.get();
			transaction.setSourceRequestDate(transactionDto.getSourceRequestDate());
			transaction.setSourceResponseDate(transactionDto.getSourceResponseDate());
			transaction.setFromCurrency(transactionDto.getFromCurrency());
			transaction.setToCurrency(transactionDto.getToCurrency());
			transaction.setExchangeRate(transactionDto.getExchangeRate());
			transaction.setSourceAmount(transactionDto.getSourceAmount());
			transaction.setTargetAmount(transactionDto.getTargetAmount());
			transaction.setStatus(transactionDto.getStatus());
			transaction.setType(transactionDto.getType());
			transaction.setCurrencySource(transactionDto.getCurrencySource());
			return transactionRepository.save(transaction);
		}
		return null;
	}

	@Override
	public Transaction updateTransaction(String id, TransactionStatus transactionStatus) {
		Optional<Transaction> optionalTransaction = transactionRepository.findById(id);
		if (optionalTransaction.isPresent()) {
			Transaction transaction = optionalTransaction.get();
			transaction.setStatus(transactionStatus);
			return transactionRepository.save(transaction);
		}
		return null;
	}

}
