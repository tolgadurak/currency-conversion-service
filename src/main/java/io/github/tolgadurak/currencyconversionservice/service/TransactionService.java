package io.github.tolgadurak.currencyconversionservice.service;

import java.util.Date;
import java.util.List;

import io.github.tolgadurak.currencyconversionservice.domain.Transaction;
import io.github.tolgadurak.currencyconversionservice.domain.enums.TransactionStatus;
import io.github.tolgadurak.currencyconversionservice.dto.TransactionDto;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public interface TransactionService {
	TransactionDto getById(String id);

	List<TransactionDto> getAllByCreatedDateBetween(Date startDate, Date endDate);

	Transaction addTransaction(TransactionDto transactionDto);

	Transaction updateTransaction(String id, TransactionDto transactionDto);
	
	Transaction updateTransaction(String id, TransactionStatus transactionStatus);
}
