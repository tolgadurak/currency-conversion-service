package io.github.tolgadurak.currencyconversionservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import io.github.tolgadurak.currencyconversionservice.CurrencyConversionServiceAppConfig;
import io.github.tolgadurak.currencyconversionservice.service.CurrencyConversionService;
import io.github.tolgadurak.currencyconversionservice.util.JsonConverter;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public abstract class BaseCurrencyConversionService implements CurrencyConversionService {
	protected CurrencyConversionServiceAppConfig appConfig;
	protected JsonConverter jsonConverter;

	@Autowired
	public BaseCurrencyConversionService(CurrencyConversionServiceAppConfig appConfig, JsonConverter jsonConverter) {
		this.appConfig = appConfig;
		this.jsonConverter = jsonConverter;

	}

	public CurrencyConversionServiceAppConfig getAppConfig() {
		return appConfig;
	}

}
