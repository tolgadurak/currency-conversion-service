package io.github.tolgadurak.currencyconversionservice.api.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import io.github.tolgadurak.currencyconversionservice.domain.enums.Currency;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class ExchangeCalculationQueryDTO {
	@NotNull(message = "source is required")
	private Currency source;
	@NotNull(message = "target is required")
	private Currency target;
	@NotNull(message = "amount is required")
	private BigDecimal amount;

	public Currency getSource() {
		return source;
	}

	public Currency getTarget() {
		return target;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setSource(Currency source) {
		this.source = source;
	}

	public void setTarget(Currency target) {
		this.target = target;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
