package io.github.tolgadurak.currencyconversionservice.api.valueobject;

import java.io.Serializable;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public interface ExternalServiceResponseVO extends Serializable {

}
