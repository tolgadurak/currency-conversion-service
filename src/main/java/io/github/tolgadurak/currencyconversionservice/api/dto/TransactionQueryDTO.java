package io.github.tolgadurak.currencyconversionservice.api.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import io.github.tolgadurak.currencyconversionservice.validation.ValidTransactionQueryDTO;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@ValidTransactionQueryDTO
public class TransactionQueryDTO {
	private String id;
	@DateTimeFormat(iso = ISO.DATE_TIME)
	@ApiModelProperty(example = "2020-08-01T00:00:00.000+03:00")
	private Date startDate;
	@DateTimeFormat(iso = ISO.DATE_TIME)
	@ApiModelProperty(example = "2020-08-02T00:00:00.000+03:00")
	private Date endDate;

	public String getId() {
		return id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
