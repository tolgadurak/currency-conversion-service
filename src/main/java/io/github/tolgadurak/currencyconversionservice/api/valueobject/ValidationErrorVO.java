package io.github.tolgadurak.currencyconversionservice.api.valueobject;

import io.github.tolgadurak.currencyconversionservice.domain.enums.ValidationErrorType;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class ValidationErrorVO {
	private String violatorParam;
	private String errorMessage;
	private ValidationErrorType type;

	public ValidationErrorVO(Builder builder) {
		this.violatorParam = builder.violatorParam;
		this.errorMessage = builder.errorMessage;
		this.type = builder.type;
	}

	public String getViolatorParam() {
		return violatorParam;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public ValidationErrorType getType() {
		return type;
	}

	public void setViolatorParam(String violatorParam) {
		this.violatorParam = violatorParam;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setType(ValidationErrorType type) {
		this.type = type;
	}

	public static class Builder {
		private String violatorParam;
		private String errorMessage;
		private ValidationErrorType type;

		public Builder violatorParam(String violatorParam) {
			this.violatorParam = violatorParam;
			return this;
		}

		public Builder errorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
			return this;
		}

		public Builder type(ValidationErrorType type) {
			this.type = type;
			return this;
		}

		public ValidationErrorVO build() {
			return new ValidationErrorVO(this);
		}
	}
}
