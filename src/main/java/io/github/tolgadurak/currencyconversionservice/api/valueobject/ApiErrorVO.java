package io.github.tolgadurak.currencyconversionservice.api.valueobject;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.github.tolgadurak.currencyconversionservice.domain.enums.ErrorType;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@JsonInclude(Include.NON_NULL)
public class ApiErrorVO extends ApiVO {
	private String errorId;
	private String errorMessage;
	private Date errorDate;
	private List<ValidationErrorVO> validationErrors;
	private ExternalServiceResponseVO externalServiceResponse;
	private ErrorType errorType;

	public ApiErrorVO() {
		super(false);
	}

	public ApiErrorVO(Builder builder) {
		super(false);
		this.errorId = builder.errorId;
		this.errorMessage = builder.errorMessage;
		this.errorDate = builder.errorDate;
		this.validationErrors = builder.validationErrors;
		this.externalServiceResponse = builder.externalServiceResponse;
		this.errorType = builder.errorType;
	}

	public String getErrorId() {
		return errorId;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public Date getErrorDate() {
		return errorDate;
	}

	public List<ValidationErrorVO> getValidationErrors() {
		return validationErrors;
	}

	public ExternalServiceResponseVO getExternalServiceResponse() {
		return externalServiceResponse;
	}

	public ErrorType getErrorType() {
		return errorType;
	}

	public static class Builder {
		private String errorId;
		private String errorMessage;
		private Date errorDate;
		private List<ValidationErrorVO> validationErrors;
		private ExternalServiceResponseVO externalServiceResponse;
		private ErrorType errorType;

		public Builder errorId(String errorId) {
			this.errorId = errorId;
			return this;
		}

		public Builder errorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
			return this;
		}

		public Builder errorDate(Date errorDate) {
			this.errorDate = errorDate;
			return this;
		}

		public Builder validationErrors(List<ValidationErrorVO> validationErrors) {
			this.validationErrors = validationErrors;
			return this;
		}

		public Builder externalServiceResponse(ExternalServiceResponseVO externalServiceResponse) {
			this.externalServiceResponse = externalServiceResponse;
			return this;
		}

		public Builder errorType(ErrorType errorType) {
			this.errorType = errorType;
			return this;
		}

		public ApiErrorVO build() {
			return new ApiErrorVO(this);
		}
	}
}
