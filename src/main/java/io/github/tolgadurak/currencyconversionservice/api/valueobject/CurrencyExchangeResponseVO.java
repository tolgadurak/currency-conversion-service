package io.github.tolgadurak.currencyconversionservice.api.valueobject;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.github.tolgadurak.currencyconversionservice.domain.enums.Currency;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@JsonInclude(Include.NON_NULL)
public class CurrencyExchangeResponseVO extends ApiVO {
	private Currency source;
	private Currency target;
	private BigDecimal sourceAmount;
	private BigDecimal rate;
	private BigDecimal resultAmount;

	public CurrencyExchangeResponseVO() {
		super(true);
	}

	public CurrencyExchangeResponseVO(Builder builder) {
		super(true);
		this.source = builder.source;
		this.target = builder.target;
		this.sourceAmount = builder.sourceAmount;
		this.rate = builder.rate;
		this.resultAmount = builder.resultAmount;
	}

	public boolean isSuccess() {
		return success;
	}

	public Currency getSource() {
		return source;
	}

	public Currency getTarget() {
		return target;
	}

	public BigDecimal getSourceAmount() {
		return sourceAmount;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public BigDecimal getResultAmount() {
		return resultAmount;
	}

	public static class Builder {
		private Currency source;
		private Currency target;
		private BigDecimal sourceAmount;
		private BigDecimal rate;
		private BigDecimal resultAmount;

		public Builder source(Currency source) {
			this.source = source;
			return this;
		}

		public Builder target(Currency target) {
			this.target = target;
			return this;
		}

		public Builder sourceAmount(BigDecimal sourceAmount) {
			this.sourceAmount = sourceAmount;
			return this;
		}

		public Builder rate(BigDecimal rate) {
			this.rate = rate;
			return this;
		}

		public Builder resultAmount(BigDecimal resultAmount) {
			this.resultAmount = resultAmount;
			return this;
		}

		public CurrencyExchangeResponseVO build() {
			return new CurrencyExchangeResponseVO(this);
		}
	}

}
