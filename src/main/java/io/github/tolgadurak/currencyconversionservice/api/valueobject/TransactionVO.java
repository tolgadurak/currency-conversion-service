package io.github.tolgadurak.currencyconversionservice.api.valueobject;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.github.tolgadurak.currencyconversionservice.domain.enums.Currency;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@JsonInclude(Include.NON_NULL)
public class TransactionVO {
	private String status;
	private String transactionId;
	private Date transactionDate;
	private Currency fromCurrency;
	private Currency toCurrency;
	private BigDecimal exchangeRate;
	private BigDecimal sourceAmount;
	private BigDecimal targetAmount;
	private Date sourceRequestDate;
	private Date sourceResponseDate;
	private String currencySource;

	public TransactionVO() {
		// Default constructor
	}

	public TransactionVO(Builder builder) {
		this.status = builder.status;
		this.transactionId = builder.transactionId;
		this.fromCurrency = builder.fromCurrency;
		this.toCurrency = builder.toCurrency;
		this.exchangeRate = builder.exchangeRate;
		this.sourceAmount = builder.sourceAmount;
		this.targetAmount = builder.targetAmount;
		this.transactionDate = builder.transactionDate;
		this.sourceRequestDate = builder.sourceRequestDate;
		this.sourceResponseDate = builder.sourceResponseDate;
		this.currencySource = builder.currencySource;
	}

	public String getStatus() {
		return status;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public Date getSourceRequestDate() {
		return sourceRequestDate;
	}

	public Date getSourceResponseDate() {
		return sourceResponseDate;
	}

	public Currency getFromCurrency() {
		return fromCurrency;
	}

	public Currency getToCurrency() {
		return toCurrency;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public BigDecimal getSourceAmount() {
		return sourceAmount;
	}

	public BigDecimal getTargetAmount() {
		return targetAmount;
	}

	public String getCurrencySource() {
		return currencySource;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public void setSourceRequestDate(Date sourceRequestDate) {
		this.sourceRequestDate = sourceRequestDate;
	}

	public void setSourceResponseDate(Date sourceResponseDate) {
		this.sourceResponseDate = sourceResponseDate;
	}

	public void setFromCurrency(Currency fromCurrency) {
		this.fromCurrency = fromCurrency;
	}

	public void setToCurrency(Currency toCurrency) {
		this.toCurrency = toCurrency;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public void setSourceAmount(BigDecimal sourceAmount) {
		this.sourceAmount = sourceAmount;
	}

	public void setTargetAmount(BigDecimal targetAmount) {
		this.targetAmount = targetAmount;
	}

	public void setCurrencySource(String currencySource) {
		this.currencySource = currencySource;
	}

	public static class Builder {
		private String status;
		private String transactionId;
		private Date transactionDate;
		private Date sourceRequestDate;
		private Date sourceResponseDate;
		private Currency fromCurrency;
		private Currency toCurrency;
		private BigDecimal exchangeRate;
		private BigDecimal sourceAmount;
		private BigDecimal targetAmount;
		private String currencySource;

		public Builder status(String status) {
			this.status = status;
			return this;
		}

		public Builder transactionId(String transactionId) {
			this.transactionId = transactionId;
			return this;
		}

		public Builder transactionDate(Date transactionDate) {
			this.transactionDate = transactionDate;
			return this;
		}

		public Builder sourceRequestDate(Date sourceRequestDate) {
			this.sourceRequestDate = sourceRequestDate;
			return this;
		}

		public Builder sourceResponseDate(Date sourceResponseDate) {
			this.sourceResponseDate = sourceResponseDate;
			return this;
		}

		public Builder fromCurrency(Currency fromCurrency) {
			this.fromCurrency = fromCurrency;
			return this;
		}

		public Builder toCurrency(Currency toCurrency) {
			this.toCurrency = toCurrency;
			return this;
		}

		public Builder exchangeRate(BigDecimal exchangeRate) {
			this.exchangeRate = exchangeRate;
			return this;
		}

		public Builder sourceAmount(BigDecimal sourceAmount) {
			this.sourceAmount = sourceAmount;
			return this;
		}

		public Builder targetAmount(BigDecimal targetAmount) {
			this.targetAmount = targetAmount;
			return this;
		}

		public Builder currencySource(String currencySource) {
			this.currencySource = currencySource;
			return this;
		}

		public TransactionVO build() {
			return new TransactionVO(this);
		}
	}

}
