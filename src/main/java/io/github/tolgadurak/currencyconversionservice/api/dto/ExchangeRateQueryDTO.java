package io.github.tolgadurak.currencyconversionservice.api.dto;

import javax.validation.constraints.NotNull;

import io.github.tolgadurak.currencyconversionservice.domain.enums.Currency;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class ExchangeRateQueryDTO {
	@NotNull(message = "source is required")
	private Currency source;
	@NotNull(message = "target is required")
	private Currency target;

	public Currency getSource() {
		return source;
	}

	public Currency getTarget() {
		return target;
	}

	public void setSource(Currency source) {
		this.source = source;
	}

	public void setTarget(Currency target) {
		this.target = target;
	}

}
