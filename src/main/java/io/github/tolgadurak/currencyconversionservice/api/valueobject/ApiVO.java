package io.github.tolgadurak.currencyconversionservice.api.valueobject;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class ApiVO {
	protected Boolean success;

	public ApiVO(Boolean success) {
		this.success = success;
	}

	public Boolean getSuccess() {
		return success;
	}

}
