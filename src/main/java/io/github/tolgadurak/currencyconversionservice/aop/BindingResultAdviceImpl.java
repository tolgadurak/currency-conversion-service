package io.github.tolgadurak.currencyconversionservice.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import io.github.tolgadurak.currencyconversionservice.exception.InvalidRequestException;
/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@Aspect
@Component
public class BindingResultAdviceImpl {

	@Around("@annotation(BindingResultAdvice)")
	public Object applyAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
		Object[] args = joinPoint.getArgs();
		BindingResult bindingResult = null;
		for (Object object : args) {
			if (object instanceof BindingResult) {
				bindingResult = (BindingResult) object;
			}
		}
		if (bindingResult != null && bindingResult.hasErrors()) {
			throw new InvalidRequestException("Invalid request", bindingResult.getFieldErrors(),
					bindingResult.getGlobalErrors());
		}
		return joinPoint.proceed();
	}
}
