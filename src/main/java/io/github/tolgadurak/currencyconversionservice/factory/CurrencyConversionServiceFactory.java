package io.github.tolgadurak.currencyconversionservice.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import io.github.tolgadurak.currencyconversionservice.CurrencyConversionServiceAppConfig;
import io.github.tolgadurak.currencyconversionservice.domain.enums.CurrencyConversionServiceKey;
import io.github.tolgadurak.currencyconversionservice.service.CurrencyConversionService;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@Service
public class CurrencyConversionServiceFactory {
	private CurrencyConversionService fixerCurrencyConversionService;
	private CurrencyConversionServiceAppConfig appConfig;

	@Autowired
	public CurrencyConversionServiceFactory(
			@Qualifier(value = "fixerCurrencyConversionService") CurrencyConversionService fixerCurrencyConversionService,
			CurrencyConversionServiceAppConfig appConfig) {
		this.fixerCurrencyConversionService = fixerCurrencyConversionService;
		this.appConfig = appConfig;
	}

	public CurrencyConversionService get() {
		CurrencyConversionServiceKey serviceKey = CurrencyConversionServiceKey.fromString(appConfig.getKey());
		switch (serviceKey) {
		case FIXER:
			return fixerCurrencyConversionService;
		default:
			throw new IllegalArgumentException();
		}
	}
}
