package io.github.tolgadurak.currencyconversionservice.exception;

import io.github.tolgadurak.currencyconversionservice.api.valueobject.ExternalServiceResponseVO;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class ExternalServiceFailureException extends RuntimeException {

	private static final long serialVersionUID = 4595331392696757814L;
	private final ExternalServiceResponseVO externalServiceResponseVO;
	private final String transactionId;

	public ExternalServiceFailureException(String message, ExternalServiceResponseVO externalServiceResponseVO,
			String transactionId) {
		super(message);
		this.externalServiceResponseVO = externalServiceResponseVO;
		this.transactionId = transactionId;
	}

	public ExternalServiceResponseVO getExternalServiceResponseVO() {
		return externalServiceResponseVO;
	}

	public String getTransactionId() {
		return transactionId;
	}

}
