package io.github.tolgadurak.currencyconversionservice.exception;

import java.util.List;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class InvalidRequestException extends RuntimeException {

	private static final long serialVersionUID = 3693221881536261902L;

	private final List<FieldError> fieldErrors;
	private final List<ObjectError> globalErrors;

	public InvalidRequestException(String message, List<FieldError> fieldErrors, List<ObjectError> globalErrors) {
		super(message);
		this.fieldErrors = fieldErrors;
		this.globalErrors = globalErrors;
	}

	public List<FieldError> getFieldErrors() {
		return fieldErrors;
	}

	public List<ObjectError> getGlobalErrors() {
		return globalErrors;
	}

}
