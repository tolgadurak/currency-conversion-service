package io.github.tolgadurak.currencyconversionservice.exception;

import java.io.IOException;
/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class UncheckedIOException extends RuntimeException {

	private static final long serialVersionUID = 6206266039401226143L;
	private final String transactionId;

	public UncheckedIOException(String message, IOException e, String transactionId) {
		super(message, e);
		this.transactionId = transactionId;
	}

	public String getTransactionId() {
		return transactionId;
	}

}
