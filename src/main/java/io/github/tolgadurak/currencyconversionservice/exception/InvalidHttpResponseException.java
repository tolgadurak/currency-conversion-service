package io.github.tolgadurak.currencyconversionservice.exception;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class InvalidHttpResponseException extends RuntimeException {

	private static final long serialVersionUID = 7948850850344216820L;

	private final String transactionId;

	public InvalidHttpResponseException(String message, String transcationId) {
		super(message);
		this.transactionId = transcationId;
	}

	public String getTransactionId() {
		return transactionId;
	}

}
