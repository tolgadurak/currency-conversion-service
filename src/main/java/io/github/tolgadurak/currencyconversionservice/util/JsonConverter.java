package io.github.tolgadurak.currencyconversionservice.util;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class JsonConverter {
	private ObjectMapper objectMapper;
	private static final Logger logger = LogManager.getLogger(JsonConverter.class);

	@PostConstruct
	private void setup() {
		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public <T> T deserialize(String data, Class<T> clazz) {
		try {
			return objectMapper.readValue(data, clazz);
		} catch (JsonProcessingException e) {
			logger.error("An error occurred while deserializing json", e);
		}
		return null;
	}

	public String serialize(Object object) {
		try {
			return objectMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			logger.error("An error occurred while serializing json", e);
		}
		return null;
	}
}
