package io.github.tolgadurak.currencyconversionservice.util;
/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public abstract class HttpClientCommons {
	public static final String SLASH = "/";
	public static final String QUESTION_MARK = "?";
	public static final String AMPERSAND = "&";
	public static final String EQUAL = "=";

	private HttpClientCommons() {
		throw new UnsupportedOperationException();
	}
}
