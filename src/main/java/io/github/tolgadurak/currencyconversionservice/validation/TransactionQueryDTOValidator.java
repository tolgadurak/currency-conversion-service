package io.github.tolgadurak.currencyconversionservice.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import io.github.tolgadurak.currencyconversionservice.api.dto.TransactionQueryDTO;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public class TransactionQueryDTOValidator
		implements ConstraintValidator<ValidTransactionQueryDTO, TransactionQueryDTO> {

	@Override
	public boolean isValid(TransactionQueryDTO transactionQueryDTO, ConstraintValidatorContext context) {
		return StringUtils.isNotBlank(transactionQueryDTO.getId()) || (transactionQueryDTO.getStartDate() != null
				&& transactionQueryDTO.getEndDate() != null);
	}

}
