package io.github.tolgadurak.currencyconversionservice.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@Documented
@Constraint(validatedBy = TransactionQueryDTOValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidTransactionQueryDTO {
	String message() default "Invalid transaction query request";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
