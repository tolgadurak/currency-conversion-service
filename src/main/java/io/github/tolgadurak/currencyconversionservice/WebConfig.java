package io.github.tolgadurak.currencyconversionservice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.github.tolgadurak.currencyconversionservice.util.JsonConverter;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@Configuration
public class WebConfig {
	@Bean
	public JsonConverter jsonConverter() {
		return new JsonConverter();
	}
}
