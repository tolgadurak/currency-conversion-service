package io.github.tolgadurak.currencyconversionservice.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import io.github.tolgadurak.currencyconversionservice.domain.Transaction;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
public interface TransactionRepository extends CrudRepository<Transaction, String> {
	Optional<Transaction> findById(String id);

	List<Transaction> findAllByCreatedDateBetween(Date createdDateStart, Date createdDateEnd);
}
