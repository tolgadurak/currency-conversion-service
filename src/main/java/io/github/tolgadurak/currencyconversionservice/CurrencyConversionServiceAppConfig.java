package io.github.tolgadurak.currencyconversionservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@Configuration
public class CurrencyConversionServiceAppConfig {
	@Value("${currency.conversion.service.key:FIXER}")
	private String key;
	@Value("${currency.conversion.service.url:http://data.fixer.io/api}")
	private String baseUrl;
	@Value("${currency.conversion.service.accessKey}")
	private String accessKey;
	@Value("${currency.conversion.service.paidVersion:false}")
	private boolean paidVersion;

	public String getKey() {
		return key;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public boolean isPaidVersion() {
		return paidVersion;
	}

}
