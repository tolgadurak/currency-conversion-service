package io.github.tolgadurak.currencyconversionservice.controller;

import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.tolgadurak.currencyconversionservice.aop.BindingResultAdvice;
import io.github.tolgadurak.currencyconversionservice.api.dto.ExchangeCalculationQueryDTO;
import io.github.tolgadurak.currencyconversionservice.api.dto.ExchangeRateQueryDTO;
import io.github.tolgadurak.currencyconversionservice.api.dto.TransactionQueryDTO;
import io.github.tolgadurak.currencyconversionservice.api.valueobject.TransactionVO;
import io.github.tolgadurak.currencyconversionservice.domain.Transaction;
import io.github.tolgadurak.currencyconversionservice.domain.enums.TransactionStatus;
import io.github.tolgadurak.currencyconversionservice.domain.enums.TransactionType;
import io.github.tolgadurak.currencyconversionservice.dto.CurrencyExchangeResponseDto;
import io.github.tolgadurak.currencyconversionservice.dto.TransactionDto;
import io.github.tolgadurak.currencyconversionservice.factory.CurrencyConversionServiceFactory;
import io.github.tolgadurak.currencyconversionservice.service.ApiValueObjectService;
import io.github.tolgadurak.currencyconversionservice.service.CurrencyConversionService;
import io.github.tolgadurak.currencyconversionservice.service.TransactionService;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@RestController
@RequestMapping("/api/v1")
public class ApiControllerV1 {

	private TransactionService transactionService;
	private CurrencyConversionServiceFactory currencyConversionServiceFactory;
	private ApiValueObjectService apiValueObjectService;

	@Autowired
	public ApiControllerV1(TransactionService transactionService,
			CurrencyConversionServiceFactory currencyConversionServiceFactory,
			ApiValueObjectService apiValueObjectService) {
		this.transactionService = transactionService;
		this.currencyConversionServiceFactory = currencyConversionServiceFactory;
		this.apiValueObjectService = apiValueObjectService;
	}

	@GetMapping("/query-exchange-rate")
	@BindingResultAdvice
	public TransactionVO queryExchangeRate(@Valid ExchangeRateQueryDTO exchangeRateQueryDTO,
			BindingResult bindingResult) throws InterruptedException {
		TransactionDto transactionDto = new TransactionDto.Builder().fromCurrency(exchangeRateQueryDTO.getSource())
				.toCurrency(exchangeRateQueryDTO.getTarget()).status(TransactionStatus.IP)
				.type(TransactionType.QUERYCURRENCYRATE).build();
		Transaction transaction = transactionService.addTransaction(transactionDto);
		transactionDto.setId(transaction.getId());
		CurrencyConversionService currencyConversionService = currencyConversionServiceFactory.get();
		CurrencyExchangeResponseDto currencyExchangeResponseDto = currencyConversionService
				.queryExchangeRate(transactionDto);
		if (currencyExchangeResponseDto.isSuccess()) {
			transactionDto.setStatus(TransactionStatus.AP);
		}

		transactionDto.setCreatedDate(transaction.getCreatedDate());
		transactionDto.setSourceRequestDate(currencyExchangeResponseDto.getRequestDate());
		transactionDto.setSourceResponseDate(currencyExchangeResponseDto.getResponseDate());
		transactionDto.setExchangeRate(currencyExchangeResponseDto.getRate());
		transactionDto.setCurrencySource(currencyConversionService.getSource());
		transactionService.updateTransaction(transaction.getId(), transactionDto);
		return apiValueObjectService.convert(transactionDto);
	}

	@GetMapping("/calculate-exchange")
	@BindingResultAdvice
	public TransactionVO calculateExchange(@Valid ExchangeCalculationQueryDTO exchangeCalculationQueryDTO,
			BindingResult bindingResult) throws InterruptedException {
		TransactionDto transactionDto = new TransactionDto.Builder()
				.fromCurrency(exchangeCalculationQueryDTO.getSource())
				.toCurrency(exchangeCalculationQueryDTO.getTarget())
				.sourceAmount(exchangeCalculationQueryDTO.getAmount()).status(TransactionStatus.IP)
				.type(TransactionType.EXCHANGECALCULATION).build();
		Transaction transaction = transactionService.addTransaction(transactionDto);
		transactionDto.setId(transaction.getId());
		CurrencyConversionService currencyConversionService = currencyConversionServiceFactory.get();
		CurrencyExchangeResponseDto currencyExchangeResponseDto = currencyConversionService
				.calculateTargetAmount(transactionDto);
		if (currencyExchangeResponseDto.isSuccess()) {
			transactionDto.setStatus(TransactionStatus.AP);
		}

		transactionDto.setCreatedDate(transaction.getCreatedDate());
		transactionDto.setSourceRequestDate(currencyExchangeResponseDto.getRequestDate());
		transactionDto.setSourceResponseDate(currencyExchangeResponseDto.getResponseDate());
		transactionDto.setTargetAmount(currencyExchangeResponseDto.getResultAmount());
		transactionDto.setCurrencySource(currencyConversionService.getSource());
		transactionService.updateTransaction(transaction.getId(), transactionDto);
		return apiValueObjectService.convert(transactionDto);
	}

	@GetMapping("/transaction")
	@BindingResultAdvice
	public List<TransactionVO> queryTransactionByIdOrDate(@Valid TransactionQueryDTO transactionQuery,
			BindingResult bindingResult) {
		if (StringUtils.isNotBlank(transactionQuery.getId())) {
			TransactionDto transactionDto = transactionService.getById(transactionQuery.getId());
			return transactionDto != null ? apiValueObjectService.convert(Arrays.asList(transactionDto))
					: Arrays.asList();
		} else {
			List<TransactionDto> transactionDtos = transactionService
					.getAllByCreatedDateBetween(transactionQuery.getStartDate(), transactionQuery.getEndDate());
			return apiValueObjectService.convert(transactionDtos);
		}
	}

}
