package io.github.tolgadurak.currencyconversionservice.controller.advice;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.github.tolgadurak.currencyconversionservice.api.valueobject.ApiErrorVO;
import io.github.tolgadurak.currencyconversionservice.api.valueobject.ExternalServiceResponseVO;
import io.github.tolgadurak.currencyconversionservice.api.valueobject.ValidationErrorVO;
import io.github.tolgadurak.currencyconversionservice.domain.enums.ErrorType;
import io.github.tolgadurak.currencyconversionservice.domain.enums.TransactionStatus;
import io.github.tolgadurak.currencyconversionservice.domain.enums.ValidationErrorType;
import io.github.tolgadurak.currencyconversionservice.exception.ExternalServiceFailureException;
import io.github.tolgadurak.currencyconversionservice.exception.InvalidHttpResponseException;
import io.github.tolgadurak.currencyconversionservice.exception.InvalidRequestException;
import io.github.tolgadurak.currencyconversionservice.exception.UncheckedIOException;
import io.github.tolgadurak.currencyconversionservice.service.TransactionService;

/**
 * 
 * @author Tolga Durak <tolgadurak.dev@gmail.com>
 *
 */
@ControllerAdvice
public class ApiControllerV1Advice {

	private static final Logger logger = LogManager.getLogger(ApiControllerV1Advice.class);

	@Autowired
	private TransactionService transactionService;

	@ResponseBody
	@ExceptionHandler(InvalidRequestException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	public ApiErrorVO handleInvalidRequestException(InvalidRequestException e) {
		List<FieldError> fieldErrors = e.getFieldErrors();
		List<ObjectError> globalErrors = e.getGlobalErrors();
		List<ValidationErrorVO> validationErrors = fieldErrors.stream()
				.map(error -> new ValidationErrorVO.Builder().violatorParam(error.getField())
						.errorMessage(error.getDefaultMessage()).type(ValidationErrorType.PARAMETER).build())
				.collect(Collectors.toList());
		globalErrors.forEach(error -> validationErrors.add(new ValidationErrorVO.Builder()
				.errorMessage(error.getDefaultMessage()).type(ValidationErrorType.GLOBAL).build()));
		ApiErrorVO apiErrorVO = new ApiErrorVO.Builder().errorId(UUID.randomUUID().toString())
				.errorMessage(e.getMessage()).errorDate(new Date()).validationErrors(validationErrors)
				.errorType(ErrorType.VALIDATION).build();
		String errorLog = String.format("Error message: %s, Error Id: %s", apiErrorVO.getErrorMessage(),
				apiErrorVO.getErrorId());
		logger.error(errorLog, e);
		return apiErrorVO;
	}

	@ResponseBody
	@ExceptionHandler(InvalidHttpResponseException.class)
	@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
	public ApiErrorVO handleInvalidHttpResponseException(InvalidHttpResponseException e) {
		transactionService.updateTransaction(e.getTransactionId(), TransactionStatus.FA);
		ApiErrorVO apiErrorVO = new ApiErrorVO.Builder().errorId(UUID.randomUUID().toString())
				.errorMessage(e.getMessage()).errorDate(new Date()).errorType(ErrorType.INVALID_HTTP_RESPONSE).build();
		String errorLog = String.format("Error message: %s, Error Id: %s", apiErrorVO.getErrorMessage(),
				apiErrorVO.getErrorId());
		logger.error(errorLog, e);
		return apiErrorVO;
	}

	@ResponseBody
	@ExceptionHandler(UncheckedIOException.class)
	@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
	public ApiErrorVO handleUncheckedIOException(UncheckedIOException e) {
		transactionService.updateTransaction(e.getTransactionId(), TransactionStatus.FA);
		ApiErrorVO apiErrorVO = new ApiErrorVO.Builder().errorId(UUID.randomUUID().toString())
				.errorMessage(e.getMessage()).errorDate(new Date()).errorType(ErrorType.HTTP_IO_EXCEPTION).build();
		String errorLog = String.format("Error message: %s, Error Id: %s", apiErrorVO.getErrorMessage(),
				apiErrorVO.getErrorId());
		logger.error(errorLog, e);
		return apiErrorVO;
	}

	@ResponseBody
	@ExceptionHandler(ExternalServiceFailureException.class)
	@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
	public ApiErrorVO handleExternalServiceFailureException(ExternalServiceFailureException e) {
		transactionService.updateTransaction(e.getTransactionId(), TransactionStatus.FA);
		ExternalServiceResponseVO externalServiceResponse = e.getExternalServiceResponseVO();
		ApiErrorVO apiErrorVO = new ApiErrorVO.Builder().errorId(UUID.randomUUID().toString())
				.errorMessage(e.getMessage()).errorDate(new Date()).externalServiceResponse(externalServiceResponse)
				.errorType(ErrorType.EXTERNAL_SERVICE).build();
		String errorLog = String.format("Error message: %s, Error Id: %s", apiErrorVO.getErrorMessage(),
				apiErrorVO.getErrorId());
		logger.error(errorLog, e);
		return apiErrorVO;
	}
}
