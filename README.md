# currency-conversion-service

A Spring Boot application for retrieving currency exchange rates and calculation of exchange via REST API implementing various currency data providers.

##### Prerequisites
* JDK 11
* Maven 3.6.3


##### To start the application in attached mode you can run the command below:

```
mvn spring-boot:run
```

###### Default application port: `8080`
###### Default application path: `/currency-conversion-service`

##### Features in API V1

###### Base path: `/api/v1`

* Query Exchange Rate: `/query-exchange-rate`

* Calculate Exchange: `/calculate-exchange`

* Query Transaction By Id Or Date: `/transaction`

##### Swagger UI

The application contains Swagger UI for REST API documentation. It can be accessed via the URL below:

```
http://localhost:8080/currency-conversion-service/swagger-ui.html
```

##### Configuration

Configuration is provided by an external application.properties file that can contain the following options:

* currency.conversion.service.key
    * Available options: FIXER   
* currency.conversion.service.url
    * Available options: http://data.fixer.io/api     
* currency.conversion.service.accessKey
    * Available options: Your API key   
* currency.conversion.service.paidVersion
     * Available options: true/false

For example:

```
currency.conversion.service.key=FIXER
currency.conversion.service.url=http://data.fixer.io/api
currency.conversion.service.accessKey=
currency.conversion.service.paidVersion=false
```

##### Currency data is provided via following providers:
* [Fixer.io](https://fixer.io/)
     * You need to provide your API key to integrate to currency data provider



